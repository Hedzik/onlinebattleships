﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class RegisterPlayerRequestArgs
    {
        public string Nickname { get; set; }
    }

    public class RegisterPlayerResponseArgs
    {
        public int AssociatedPlayerId { get; set; }
    }
}
