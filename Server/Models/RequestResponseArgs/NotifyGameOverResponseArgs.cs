﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class NotifyGameOverResponseArgs
    {
        public string Loser { get; set; }
        public string Winner { get; set; }
    }
}
