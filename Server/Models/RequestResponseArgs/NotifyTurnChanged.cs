﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    /// <summary>
    /// Basically contains information about the oponent's turn(where did he shoot, and how the client should change the cell's look)
    /// </summary>
    public class NotifyTurnChangedResponseArgs
    {
        /// <summary>
        /// X position of cell shot by oponent
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Y position of cell shot by oponent
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// The new <see cref="CellType"/> of cell shot by the oponent
        /// </summary>
        public CellType CellType { get; set; }
    }
}
