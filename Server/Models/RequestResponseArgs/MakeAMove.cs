﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class MakeAMoveRequestArgs
    {
        public int SenderId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class MakeAMoveResponseArgs
    {
        public CellType ShotCell { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
