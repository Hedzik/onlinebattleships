﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class SurrenderRequestArgs
    {
        public int SenderId { get; set; }
    }

    public class SurrenderResponseArgs
    {
        public string Winner { get; set; }
        public string Loser { get; set; }
    }
}
