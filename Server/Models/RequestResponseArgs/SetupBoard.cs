﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class SetupPlayerBoardRequestArgs
    {
        public int SenderId { get; set; }
        public Cell[] ShipCells { get; set; }
    }
}
