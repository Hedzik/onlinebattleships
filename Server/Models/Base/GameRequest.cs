﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class GameRequest
    {
        public GameRequestOrResponseType RequestType { get; set; }
        public object Arguments { get; set; }
    }
}
