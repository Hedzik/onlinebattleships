﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class GameResponse
    {
        public GameRequestOrResponseType ResponseType { get; set; }
        public bool? Success { get; set; }
        public object Arguments { get; set; }

        public static GameResponse Error(GameRequestOrResponseType responseType)
            => new GameResponse
            {
                Success = false,
                ResponseType = responseType
            };
    }
}
