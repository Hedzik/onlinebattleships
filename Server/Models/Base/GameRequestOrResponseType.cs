﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public enum GameRequestOrResponseType : int
    {
        /// <summary>
        /// Used to register new participant in the game
        /// </summary>
        RegisterPlayer,
        /// <summary>
        /// Used to setup participant's board
        /// </summary>
        SetupBoard,
        /// <summary>
        /// Used to proceed participant's turn
        /// </summary>
        MakeAMove,
        /// <summary>
        /// Used to notify the client that he should make his move now cause it's his turn
        /// </summary>
        NotifyTurnChanged,
        /// <summary>
        /// Use to notify all players that the game has started. Then <see cref="NotifyTurnChanged"/> should be sent to the partcipant who has registered as first.
        /// </summary>
        NotifyGameStarted,
        /// <summary>
        /// Used to Surrender the game instantly
        /// </summary>
        Surrender,
        /// <summary>
        /// Used to notify players that the game is over
        /// </summary>
        NotifyGameOver,
        /// <summary>
        /// Represents information about request's not beeing valid, or server's infirmity :D
        /// In simple words - if something fails, client gonna recive this type of resonse
        /// </summary>
        Error
    }
}
