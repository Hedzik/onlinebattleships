﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Cell
    {
        public int X { get; set; }
        public int Y { get; set; }

        public CellType Type { get; set; }
    }

    public enum CellType
    {
        Empty,
        EmptyAndAlreadyShotDown,
        ShipPart,
        AlreadyShotDownShipPart
    }
}
