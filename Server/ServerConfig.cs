﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class ServerConfig
    {
        /// <summary>
        /// False if there might be more than 1 client connected from the same IP (Should be used only for debug purposes)
        /// True if only one client can be connected to the server
        /// </summary>
        public bool VerifyIp { get; set; } = false;

        /// <summary>
        /// Determines the port number which the server gonna listen at
        /// </summary>
        public int ServerPort { get; set; } = 13500;
    }

    public static class Config
    {
        private const string RelativeConfigFilePath = @"Config\config.json";
        public static ServerConfig Server { get; set; }

        public static void Load()
        {
            string absolutePath = Path.GetFullPath(RelativeConfigFilePath);
            string fileName = Path.GetFileName(absolutePath);
            string directory = absolutePath.Replace(fileName, string.Empty);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            if (!File.Exists(absolutePath))
            {
                File.Create(absolutePath);
                File.WriteAllText(absolutePath, JsonConvert.SerializeObject(new ServerConfig()));
            }

            string json = File.ReadAllText(absolutePath);
            Server = JsonConvert.DeserializeObject<ServerConfig>(json);
        }
        
    }
}
