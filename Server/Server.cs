﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            ShipsServer.Start();
        }
    }

    public class PlayerIdentity
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
    }

    public static class ShipsServer
    {

        private static int PORT = 13500;
        private static IPAddress SERVER_IP = IPAddress.Loopback;
        private static readonly Socket ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        private static readonly List<Socket> ConnectedSockets = new List<Socket>();
        private static readonly Dictionary<int, Socket> AssociatedSockets = new Dictionary<int, Socket>();
        private static GameData GameData = new GameData();
        private static Dictionary<int, IPAddress> PlayerVerificationData = new Dictionary<int, IPAddress>();

        #region Helpers

        public static void SendResponse(Socket target, GameResponse response)
        {
            string json = JsonConvert.SerializeObject(response);
            SendString(target, json);
        }

        public static void SendString(Socket targetSocket, string msg)
        {
            byte[] data = Encoding.ASCII.GetBytes(msg);
            try
            {
                targetSocket.Send(data);
            }
            catch(SocketException ex)
            {
                LogError($"Client refused reciving response.({ex.Message})");
            }
        }

        public static void LogMessage(string msg, string prefix = "INFO", ConsoleColor prefixColor = ConsoleColor.Green)
        {
            Console.ForegroundColor = prefixColor;
            Console.Write($"[{prefix.ToUpper()}] ");
            Console.ResetColor();
            Console.WriteLine(msg);
        }

        public static void LogError(string msg)
        {
            LogMessage(msg, "Error", ConsoleColor.DarkRed);
        }

        public static void LogWarning(string msg)
        {
            LogMessage(msg, "WARNING", ConsoleColor.Yellow);
        }

        private static IPAddress GetLocalIp()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIPs)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    return address;
            }
            return null;
        }

        #endregion

        public static void Start()
        {
            LogMessage("Reading config file...");
            Config.Load();
            PORT = Config.Server.ServerPort;

            SERVER_IP = GetLocalIp();
            ServerSocket.Bind(new IPEndPoint(SERVER_IP, PORT));
            ServerSocket.Listen(1);

            LogMessage($"Server is listening at {SERVER_IP}:{PORT}");

            Socket client = null;
            while (true)
            {
                client = ServerSocket.Accept();
                Thread ClientThread = new Thread(new ThreadStart(() => HandleClient(client)));
                LogMessage("New client connected!");
                ClientThread.Start();
            }
        }

        public static void HandleClient(Socket client)
        {
            ConnectedSockets.Add(client);
            if (ConnectedSockets.Count > 2)
            {
                LogError("Already 2/2 players ingame, rejecting incoming connection attempt.");
                client.Disconnect(true);
                ConnectedSockets.Remove(client);
                return;
            }

            while (true)
            {
                byte[] buffer = new byte[1024];

                try
                {
                    int recivedBytesAmount = client.Receive(buffer);
                }
                catch (SocketException ex)
                {
                    LogError($"Error while reciving data: {ex.Message}");
                    return;
                }

                string jsonRequest = Encoding.ASCII.GetString(buffer).TrimEnd('\0');

                try
                {
                    GameRequest gameRequest = JsonConvert.DeserializeObject<GameRequest>(jsonRequest);
                    DetermineActionForUserRequest(client, gameRequest);
                }
                catch (JsonException ex)
                {
                    LogError($"Error while parsing client's response: {ex.Message}");
                    GameResponse errorResponse = GameResponse.Error(GameRequestOrResponseType.Error);
                    SendString(client, JsonConvert.SerializeObject(errorResponse));
                }
            }

        }


        private static bool IsSocketAssociatedWithParticiapnt(int participantId, Socket socket)
        {
            IPAddress ip = (socket.RemoteEndPoint as IPEndPoint).Address;
            return PlayerVerificationData.ContainsKey(participantId) && PlayerVerificationData[participantId].Equals(ip);
        }

        /// <summary>
        /// Proceeds client's request.
        /// </summary>
        public static void DetermineActionForUserRequest(Socket sender, GameRequest request)
        {
            if (request == null)
            {
                LogError("The request was null!");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.Error));
                return;
            }
            //By default(idk how to turn it on) Json.DeserializeObject leaves .Arguments as JObject type so we have to cast it to (.NET) object manually
            JObject jObject = request.Arguments as JObject;
            switch (request.RequestType)
            {
                case GameRequestOrResponseType.RegisterPlayer:
                    RegisterPlayerRequestArgs registerRequestArgs = jObject.ToObject<RegisterPlayerRequestArgs>();
                    if (registerRequestArgs != null)
                    {
                        ProceedRegisterRequest(sender, registerRequestArgs);
                        return;
                    }
                    break;
                case GameRequestOrResponseType.SetupBoard:
                    SetupPlayerBoardRequestArgs setupBoardRequestArgs = jObject.ToObject<SetupPlayerBoardRequestArgs>();
                    if (setupBoardRequestArgs != null)
                    {
                        ProceedSetupBoardRequest(sender, setupBoardRequestArgs);
                        return;
                    }
                    break;
                case GameRequestOrResponseType.MakeAMove:
                    MakeAMoveRequestArgs shootCellRequestArgs = jObject.ToObject<MakeAMoveRequestArgs>();
                    if (shootCellRequestArgs != null)
                    {
                        ProceedMakeAMoveRequest(sender, shootCellRequestArgs);
                        return;
                    }
                    break;
                case GameRequestOrResponseType.Surrender:
                    SurrenderRequestArgs surrenderRequestArgs = jObject.ToObject<SurrenderRequestArgs>();
                    if (surrenderRequestArgs != null)
                    {
                        ProceedSurrenderRequest(sender, surrenderRequestArgs);
                        return;
                    }
                    break;
            }

            SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.Error));

        }

        /// <summary>
        /// Proceeds Register request
        /// </summary>
        private static void ProceedRegisterRequest(Socket sender, RegisterPlayerRequestArgs requestArgs)
        {
            IPAddress ip = (sender.RemoteEndPoint as IPEndPoint).Address;
            if (Config.Server.VerifyIp && PlayerVerificationData.ContainsValue(ip))
            {
                LogWarning($"A client attempted to register another player from the same ip. Aborting action.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.RegisterPlayer));
                return;
            }

            //If we have reached maximum amount of players, we can't register another one
            if (GameData.MaximumAmountOfPlayersReached)
            {
                LogWarning("Detected registration attempt while maximum player amount have been already reached! Aborting registration.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.RegisterPlayer));
                return;
            }
            IEnumerable<Participant> participants = GameData.Participants.Values;

            foreach (Participant p in participants)
            {
                //A player already registered with this nickname
                if (p.Identity.Nickname.ToLower() == requestArgs.Nickname.ToLower())
                {
                    LogWarning($"A player already registered as {requestArgs.Nickname}. Aborting registration");
                    SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.RegisterPlayer));
                    return;
                }
            }

            Participant participant = GameData.RegisterPlayer(requestArgs.Nickname);

            PlayerVerificationData.Add(participant.Identity.Id, ip);
            AssociatedSockets.Add(participant.Identity.Id, sender);

            LogMessage($"Successfully registered {participant.Identity.Nickname}(ID={participant.Identity.Id}) in the game");

            //Registaration successed
            SendResponse(sender, new GameResponse
            {
                ResponseType = GameRequestOrResponseType.RegisterPlayer,
                Success = true,
                Arguments = new RegisterPlayerResponseArgs
                {
                    AssociatedPlayerId = participant.Identity.Id
                }
            });

            //Send notification that the game has started!
            if (GameData.MaximumAmountOfPlayersReached && GameData.HaveEveryPlayerSetupHisBoard)
            {
                //Send notification to all sockets
                foreach (Socket socket in AssociatedSockets.Values)
                {
                    SendResponse(socket, new GameResponse
                    {
                        ResponseType = GameRequestOrResponseType.NotifyGameStarted,
                        Success = null,
                        Arguments = new NotifyGameStartedResponseArgs
                        {
                            OponentsNickname = GameData.GetOponentFor(participant.Identity.Id).Identity.Nickname
                        }
                    });
                }

                LogMessage($"The game has started!");
            }

        }

        private static void ProceedSetupBoardRequest(Socket sender, SetupPlayerBoardRequestArgs requestArgs)
        {
            if (Config.Server.VerifyIp && !IsSocketAssociatedWithParticiapnt(requestArgs.SenderId, sender))
            {
                LogWarning($"A client tried to interfere a participant which wasn't associated with his IP. Aborting action.");
                SendResponse(sender,GameResponse.Error(GameRequestOrResponseType.SetupBoard));
                return;
            }

            Participant player = GameData[requestArgs.SenderId];
            if (player == null)
            {
                LogError($"Player with given Id={requestArgs.SenderId} doesn't exists. Aborting setting up board.");
                SendResponse(sender,GameResponse.Error(GameRequestOrResponseType.SetupBoard));
                return;
            }
            else if (player.IsBoardSetup)
            {
                LogWarning($"{player.Identity.Nickname} attempted to setup his board again! Aborting setting up.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.SetupBoard));
                return;
            }

            foreach (Cell cell in requestArgs.ShipCells)
            {
                player.Board[cell.X, cell.Y] = cell;
            }
            LogMessage($"{player.Identity.Nickname} has setup his board!");
            player.IsBoardSetup = true;

            //The actual starting the game
            if (GameData.HaveEveryPlayerSetupHisBoard)
            {
                //Send notification to all sockets
                foreach (Socket socket in AssociatedSockets.Values)
                {
                    SendResponse(socket, new GameResponse
                    {
                        ResponseType = GameRequestOrResponseType.NotifyGameStarted,
                        Success = null,
                        Arguments = new NotifyGameStartedResponseArgs
                        {
                            OponentsNickname = GameData.GetOponentFor(player.Identity.Id).Identity.Nickname
                        }
                    });
                }

                LogMessage($"The game has started!");

                //The participant which request are we currenly proceeding CAN'T be the player who should start the game 
                //(cause we assume that 2 players are required to play this game - that's how "Ships" works)
                Participant oponent = GameData.GetOponentFor(player.Identity.Id);
                Socket oponentSocket = AssociatedSockets[oponent.Identity.Id];
                SendResponse(oponentSocket, new GameResponse
                {
                    ResponseType = GameRequestOrResponseType.NotifyTurnChanged,
                    Success = null,
                    Arguments = null
                });

                LogMessage($"{oponent.Identity.Nickname} is moving first!");
            }

            SendResponse(sender, new GameResponse
            {
                ResponseType = GameRequestOrResponseType.SetupBoard,
                Success = true,
            });
        }

        private static void ProceedMakeAMoveRequest(Socket sender, MakeAMoveRequestArgs requestArgs)
        {
            if (Config.Server.VerifyIp && !IsSocketAssociatedWithParticiapnt(requestArgs.SenderId, sender))
            {
                LogWarning("A Client attempted to interfere a participant which wasn't associated with his IP. Aborting action.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.MakeAMove));
                return;
            }

            Participant participant = GameData[requestArgs.SenderId];
            if (participant == null)
            {
                LogWarning($"Player with given id({participant.Identity.Id}) doesn't exists! Aborting move.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.MakeAMove));
                return;
            }

            Participant oponent = GameData.GetOponentFor(participant.Identity.Id);
            if (oponent == null || !oponent.IsBoardSetup)
            {
                LogWarning($"{participant.Identity.Nickname}(ID={participant.Identity.Id}) attempted to shoot at enemy's board while it's not setup yet! Aborting action.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.MakeAMove));
                return;
            }

            if (!GameData.IsHisTurnNow(participant.Identity.Id))
            {
                LogWarning($"{participant.Identity.Nickname}(ID={participant.Identity.Id}) attempted to make a move while it's not his turn! Aborting action.");
                SendResponse(sender, GameResponse.Error(GameRequestOrResponseType.MakeAMove));
                return;
            }

            CellType shotCellType = oponent.Board.ShootAt(requestArgs.X, requestArgs.Y);
            GameData.ChangeTurn();

            LogMessage($"{participant.Identity.Nickname}(ID={participant.Identity.Id}) has shot at X={requestArgs.X}, Y={requestArgs.Y}");

            //Notify only the oponent. Also send information about where the player has shot
            Socket oponentSocket = AssociatedSockets[oponent.Identity.Id];
            GameResponse response = new GameResponse
            {
                ResponseType = GameRequestOrResponseType.NotifyTurnChanged,
                Success = null,
                Arguments = new NotifyTurnChangedResponseArgs
                {
                    CellType = shotCellType,
                    X = requestArgs.X,
                    Y = requestArgs.Y
                }
            };
            SendResponse(oponentSocket, response);

            //Regular response
            SendResponse(sender, new GameResponse
            {
                ResponseType = GameRequestOrResponseType.MakeAMove,
                Success = true,
                Arguments = new MakeAMoveResponseArgs
                {
                    ShotCell = shotCellType,
                    X = requestArgs.X,
                    Y = requestArgs.Y
                }
            });

            //If player has shot at empty cell it makes no sense to check if someone has won.
            if (shotCellType == CellType.AlreadyShotDownShipPart && GameData.CheckHasSomeoneLost(out PlayerIdentity loser))
            {
                response = new GameResponse
                {
                    ResponseType = GameRequestOrResponseType.NotifyGameOver,
                    Success = null,
                    Arguments = new NotifyGameOverResponseArgs
                    {
                        Winner = GameData.GetOponentFor(loser.Id).Identity.Nickname,
                        Loser = loser.Nickname
                    }
                };
                foreach (Socket socket in AssociatedSockets.Values)
                {
                    SendResponse(socket, response);
                }

            }
        }

        private static void ProceedSurrenderRequest(Socket sender, SurrenderRequestArgs requestArgs)
        {
            PlayerIdentity winner = GameData.GetOponentFor(requestArgs.SenderId).Identity;
            PlayerIdentity loser = GameData[requestArgs.SenderId].Identity;

            //Notify The game is over
            foreach (Socket socket in AssociatedSockets.Values)
            {
                SendResponse(socket, new GameResponse
                {
                    ResponseType = GameRequestOrResponseType.Surrender,
                    Success = null,
                    Arguments = new SurrenderResponseArgs
                    {
                        Winner = winner.Nickname,
                        Loser = loser.Nickname
                    }
                });
            }
        }
    }

    class Participant
    {
        public PlayerIdentity Identity { get; set; }
        public Board Board { get; set; }
        public bool IsBoardSetup { get; set; } = false;
    }

    class ClientIdentity
    {
        public IPAddress IpAddress { get; set; }
        public int AssociatedParticipantId { get; set; }
    }

    class GameData
    {
        private Queue<int> TurnsOrder { get; set; } = new Queue<int>();
        public Dictionary<int, Participant> Participants { get; private set; } = new Dictionary<int, Participant>();

        public bool MaximumAmountOfPlayersReached => Participants.Count > 1;
        public bool HaveEveryPlayerSetupHisBoard
        {
            get
            {
                if (!MaximumAmountOfPlayersReached)
                    return false;
                foreach (var p in Participants.Values)
                {
                    if (!p.IsBoardSetup)
                        return false;
                }
                return true;
            }
        }

        public Participant this[int participantId]
        {
            get => Participants.ContainsKey(participantId) ? Participants[participantId] : null;
            set
            {
                if (Participants.ContainsKey(participantId))
                {
                    Participants[participantId] = value;
                }
            }

        }

        public Participant RegisterPlayer(string nickname)
        {
            int id = Participants.LastOrDefault().Key + 1;
            Participant participant = new Participant { Identity = new PlayerIdentity { Nickname = nickname, Id = id }, Board = Board.GenerateEmptyBoard(10,10) };
            TurnsOrder.Enqueue(id);

            Participants.Add(id, participant);
            return participant;
        }

        public Participant GetOponentFor(int participantId) 
            => Participants.SingleOrDefault(valuePair => valuePair.Key != participantId).Value;

        public void ChangeTurn()
        {
            int oldId = TurnsOrder.Dequeue();
            TurnsOrder.Enqueue(oldId);
        }

        public bool IsHisTurnNow(int participantId)
           => Participants.ContainsKey(participantId) && TurnsOrder.Peek() == participantId;
        
        public bool CheckHasSomeoneLost(out PlayerIdentity loser)
        {
            loser = null;
            foreach (Participant participant in Participants.Values)
            {
                if (participant.Board.ShipCellsAmount == 0)
                {
                    loser = participant.Identity;
                    return true;
                }
                    
            }
            return false;
        }
    }

    class Board
    {
        public CellType[,] Cells { get; private set; }

        public int ShipCellsAmount { get; private set; }

        public Cell this[int x,int y]
        {
            get => new Cell
            {
                Type = Cells[x, y],
                X = x,
                Y = y
            };
            set
            {
                Cells[x, y] = value.Type;
                if (value.Type == CellType.ShipPart)
                    ShipCellsAmount++;
                else if (value.Type == CellType.AlreadyShotDownShipPart)
                    ShipCellsAmount--;

            }
        }

        public CellType ShootAt(int x, int y)
        {
            CellType cell = Cells[x, y];
            switch (cell)
            {
                case CellType.Empty:
                    Cells[x, y] = CellType.EmptyAndAlreadyShotDown;
                    break;
                case CellType.ShipPart:
                    Cells[x, y] = CellType.AlreadyShotDownShipPart;
                    ShipCellsAmount--;
                    break;
            }
            return Cells[x,y];
        }

        public static Board GenerateEmptyBoard(int width, int height)
        {
            Board result = new Board();
            CellType[,] cells = new CellType[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    cells[x, y] = CellType.Empty;
                }
            }
            result.Cells = cells;
            return result;
        }
    }
}
