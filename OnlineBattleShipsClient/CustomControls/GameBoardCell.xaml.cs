﻿using OnlineBattleShipsClient.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OnlineBattleShipsClient;
using Server;

namespace OnlineBattleShipsClient
{
    public partial class GameBoardCell : UserControl
    {
        private const string ShipCell_StyleKey = "ShipCell";
        private const string EmptyCell_StyleKey = "EmptyCell";
        private const string AlreadyShotShipCell_StyleKey = "AlreadyShotShipCell";
        private const string EmptyAndShot_CellStyleKey = "EmptyAndShotCell";

        public Cell Cell { get; private set; }

        public event Action<GameBoardCell, Cell> CellClicked;

        public GameBoardCell(Cell cell)
        {
            InitializeComponent();
            DataContext = this;

            Cell = cell;
            CellButton.Click += (sender, args) =>
            {
                this.CellClicked?.Invoke(this, Cell);
            };

            RefreshUI();
        }

        public void UpdateCell(CellType newCell)
        {
            Cell = new Cell
            {
                Type = newCell,
                X = Cell.X,
                Y = Cell.Y
            };
            RefreshUI();
        }

        public void SetBorder(Thickness thickness)
        {
            this.CellButton.BorderThickness = thickness;
        } 

        public void RefreshUI()
        {
            ChangeCellLookBasedOnCellState(Cell.Type);
        }

        private void ChangeCellStyle(string styleKey)
        {
            this.Dispatcher.Invoke(() =>
            {
                Style newStyle = (Style)FindResource(styleKey);
                CellButton.Style = newStyle;
            });
        }

        private void ChangeCellLookBasedOnCellState(CellType state)
        {
            switch (state)
            {
                case CellType.ShipPart:
                    ChangeCellStyle(ShipCell_StyleKey);
                    return;
                case CellType.AlreadyShotDownShipPart:
                    ChangeCellStyle(AlreadyShotShipCell_StyleKey);
                    break;
                case CellType.EmptyAndAlreadyShotDown:
                    ChangeCellStyle(EmptyAndShot_CellStyleKey);
                    break;
                default:
                    ChangeCellStyle(EmptyCell_StyleKey);
                    break;
            }

        }


        #region HelperMethods
        /// <summary>
        /// Changes cell's image
        /// </summary>
        /// <param name="imgName">Name of the cell's image file (the resource folder is .../Images/<fileName>)</param>
        private void ChangeImage(string imgName)
        {
            CellMainImage.Source = new BitmapImage(new Uri($"pack://application:,,,/Images/{imgName}"));
        }

        /// <summary>
        /// Sets cell's image to null
        /// </summary>
        private void ResetImage()
        {
            CellMainImage.Source = null;
        }
        #endregion
    }
}
