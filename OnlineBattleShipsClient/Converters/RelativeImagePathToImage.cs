﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace OnlineBattleShipsClient.Converters
{
    public class RelativeImagePathToImage : BaseValueConverter<RelativeImagePathToImage>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => new BitmapImage(new Uri($"pack://application:,,,/Images/{(string)value}"));

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
