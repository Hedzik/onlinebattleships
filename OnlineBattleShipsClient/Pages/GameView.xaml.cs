﻿using OnlineBattleShipsClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OnlineBattleShipsClient;
using System.Net.Sockets;
using System.Net;
using Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace OnlineBattleShipsClient.Pages
{
    public partial class GameView : Page
    {
        private const int TABLE_HEIGHT = 10;
        private const int TABLE_WIDTH = 10;
        private const int TABLE_CELL_DIMENSIONS = 32;

        private GameBoardCell[,] GUI_PLAYER_CELLS;
        private GameBoardCell[,] GUI_OPONENT_CELLS;

        private void LogMessage(string msg)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.DebugMessageLog.Text = msg;
            });
        }

        public GameView(List<Cell> shipCells, PlayerIdentity identity, ShipsClient client)
        {
            InitializeComponent();
            GameViewViewModel vm = new GameViewViewModel(client, 10, 10, identity);
            vm.LogMessage += LogMessage;
            vm.UpdateCell += UpdateLocalPlayerCell;
            vm.UpdateOponentCell += UpdateOponentCell;
            this.DataContext = vm;

            InicializeGameBoards(shipCells);
            vm.SetupBoard(shipCells);
        }

        private void UpdateLocalPlayerCell(Cell cell)
        {
            GameBoardCell handler = GUI_PLAYER_CELLS[cell.X, cell.Y];
            handler.UpdateCell(cell.Type);
            handler.RefreshUI();
        }

        private void UpdateOponentCell(Cell cell)
        {
            GameBoardCell handler = GUI_OPONENT_CELLS[cell.X, cell.Y];
            handler.UpdateCell(cell.Type);
            handler.RefreshUI();
        }

        public void OnPlayersCellClicked(GameBoardCell handler, Cell cell)
            => (DataContext as GameViewViewModel).CellClicked(cell);
        

        public void OnOponentsCellClicked(GameBoardCell handler, Cell cell)
            => (DataContext as GameViewViewModel).OponentCellClicked(cell);
        

        private void InicializeGameBoards(List<Cell> shipCells)
        {
            GenerateCellsOnGrid();

            GUI_OPONENT_CELLS = new GameBoardCell[TABLE_WIDTH, TABLE_HEIGHT];
            GUI_PLAYER_CELLS = new GameBoardCell[TABLE_WIDTH, TABLE_HEIGHT];

            //Filling cells with GameBoardCell controls
            for (int x = 0; x < TABLE_WIDTH; x++)
            {
                for (int y = 0; y < TABLE_HEIGHT; y++)
                {
                    //Player board's cells
                    GameBoardCell cellHandler = new GameBoardCell(new Cell { Type = CellType.Empty, X = x, Y = y });

                    this.GameBoard.Children.Add(cellHandler);
                    Grid.SetColumn(cellHandler, y);
                    Grid.SetRow(cellHandler, x);
                    GUI_PLAYER_CELLS[x, y] = cellHandler;
                    cellHandler.UpdateCell(CellType.Empty);
                    GUI_PLAYER_CELLS[x, y].CellClicked += OnPlayersCellClicked;
                    

                    //Oponent board's cells
                    cellHandler = new GameBoardCell(new Cell { Type = CellType.Empty, X = x, Y = y});
                    this.OponentGameBoard.Children.Add(cellHandler);
                    Grid.SetColumn(cellHandler, y);
                    Grid.SetRow(cellHandler, x);
                    GUI_OPONENT_CELLS[x, y] = cellHandler;
                    cellHandler.UpdateCell(CellType.Empty);
                    GUI_OPONENT_CELLS[x, y].CellClicked += OnOponentsCellClicked;
                    
                }
            }

            //We have this "if" only for debug purposes...
            if(shipCells != null)
            {
                foreach (Cell cell in shipCells)
                {
                    GUI_PLAYER_CELLS[cell.X, cell.Y].UpdateCell(cell.Type);
                }
            }


        }

        private void GenerateCellsOnGrid()
        {
            //Creating rows
            for (int i = 0; i < TABLE_HEIGHT; i++)
            {
                this.GameBoard.RowDefinitions.Add(new RowDefinition { Height = new GridLength(TABLE_CELL_DIMENSIONS) });
                this.OponentGameBoard.RowDefinitions.Add(new RowDefinition { Height = new GridLength(TABLE_CELL_DIMENSIONS) });
            }

            //Creating columns
            for (int i = 0; i < TABLE_WIDTH; i++)
            {
                this.GameBoard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(TABLE_CELL_DIMENSIONS) });
                this.OponentGameBoard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(TABLE_CELL_DIMENSIONS) });

            }
        }
    }
}
