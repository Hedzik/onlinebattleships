﻿using OnlineBattleShipsClient.ViewModels;
using Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OnlineBattleShipsClient.Pages
{
    /// <summary>
    /// So... this class is actaully REALLY bad written, should be REPLACED with new one
    /// Essentially creates new Board where user can select cells which will be ships' parts 
    /// It DOESN'T check if ships are coliding with themselfes or it's too many ships on the board
    /// </summary>
    public partial class SetupBoardPage : Page
    {
        private const int TABLE_HEIGHT = 10;
        private const int TABLE_WIDTH = 10;
        private const int TABLE_CELL_DIMENSIONS = 32;

        private Cell[,] Cells = new Cell[TABLE_WIDTH, TABLE_HEIGHT];
        private List<Cell> ShipCells = new List<Cell>();

        private ShipsClient Client { get; set; }
        private PlayerIdentity PlayerIdentity { get; set; }

        private bool IsBoardValid
        {
            get
            {
                if(ShipCells.Count < 5)
                {
                    LogError("You need to place at least 5 ships on the board.");
                    return false;
                }
                return true;
            }
        }

        private void LogMessage(string msg)
        {
            ClearMessages();
            MessageLog.Text = msg;
        }

        private void LogError(string msg)
        {
            ClearMessages();
            ErrorLog.Text = msg;
        }

        private void ClearMessages()
        {
            ErrorLog.Text = string.Empty;
            MessageLog.Text = string.Empty;
        }

        public SetupBoardPage(PlayerIdentity identity, ShipsClient client)
        {
            InitializeComponent();
            PlayerIdentity = identity;
            Client = client;

            InicializeGameBoards();
            this.ConfirmBoard.Click += ConfirmBoardButton_Click;
        }

        private void ConfirmBoardButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsBoardValid)
            {

                LogMessage("Successed!");
                MainWindowViewModel.MAIN_WINDOW.CurrentPage = new GameView(ShipCells, PlayerIdentity,Client);
            }
        }

        private void InicializeGameBoards()
        {

            //Creating rows
            for (int i = 0; i < TABLE_HEIGHT; i++)
            {
                this.GameBoard.RowDefinitions.Add(new RowDefinition { Height = new GridLength(TABLE_CELL_DIMENSIONS) });
            }

            //Creating columns
            for (int i = 0; i < TABLE_WIDTH; i++)
            {
                this.GameBoard.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(TABLE_CELL_DIMENSIONS) });

            }

            //Filling cells with GameBoardCell controls
            for (int x = 0; x < TABLE_WIDTH; x++)
            {
                for (int y = 0; y < TABLE_HEIGHT; y++)
                {
                    Cell cell = new Cell { Type = CellType.Empty, X = x, Y = y };

                    GameBoardCell cellHandler = new GameBoardCell(cell);
                    this.GameBoard.Children.Add(cellHandler);
                    Grid.SetColumn(cellHandler, y);
                    Grid.SetRow(cellHandler, x);
                    Cells[x, y] = cell;
                    cellHandler.UpdateCell(cell.Type);
                    cellHandler.CellClicked += SetupBoardPage_CellChanged;
                }
            }

        }

        private void SetupBoardPage_CellChanged(GameBoardCell sender, Cell cell)
        {
            int x = (int)cell.X;
            int y = (int)cell.Y;
            ShipCells.Add(new Cell { Type = CellType.ShipPart, X = x, Y = y });
            sender.UpdateCell(CellType.ShipPart);
        }

        private void FinishSettingUp(object sender, RoutedEventArgs e)
        {
            MainWindowViewModel.MAIN_WINDOW.CurrentPage = new GameView(ShipCells, PlayerIdentity,Client);
        }
    }
}
