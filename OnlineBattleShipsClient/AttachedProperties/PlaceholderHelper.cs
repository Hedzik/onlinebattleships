﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OnlineBattleShipsClient.Styles
{
    /// <summary>
    /// A Helper class for PasswordBox's placeholder
    /// </summary>
    public class PlaceholderHelper : DependencyObject
    {
        public static string GetPlaceHolder(PasswordBox obj)
        {
            return (string)obj.GetValue(PlaceHolderProperty);
        }

        public static void SetPlaceHolder(PasswordBox obj, string value)
        {
            obj.SetValue(PlaceHolderProperty, value);
        }

        /// This DP basically grant us ability to get attached PasswordBox. It's something like a "hook".
        public static readonly DependencyProperty PlaceHolderProperty =
            DependencyProperty.RegisterAttached("PlaceHolder", typeof(string), typeof(PlaceholderHelper), new PropertyMetadata(string.Empty, OnPlaceHolderChanged));

        private static void OnPlaceHolderChanged(DependencyObject d, DependencyPropertyChangedEventArgs a)
        {
            if (!(d is PasswordBox))
                return;

            ((PasswordBox)d).PasswordChanged += Password_Changed;
        }

        private static void Password_Changed(object sender, RoutedEventArgs e)
        {
            var pb = (PasswordBox)sender;
            pb.SetValue(HasTextProperty, (pb.SecurePassword.Length > 0));
            var a = pb.GetValue(HasTextProperty);
        }




        public static bool GetHasText(PasswordBox obj)
        {
            return (bool)obj.GetValue(HasTextProperty);
        }

        public static void SetHasText(PasswordBox obj, bool value)
        {
            obj.SetValue(HasTextProperty, value);
        }

        public static readonly DependencyProperty HasTextProperty =
            DependencyProperty.RegisterAttached("HasText", typeof(bool), typeof(PlaceholderHelper), new PropertyMetadata(false));









    }
}
