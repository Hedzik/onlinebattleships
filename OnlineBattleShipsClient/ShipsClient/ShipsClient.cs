﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OnlineBattleShipsClient
{

    public class ShipsClient
    {
        private readonly int PORT = 13500;
        private readonly IPAddress SERVER_IP = IPAddress.Loopback;

        public Socket ClientSocket { get; } = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public ShipsClient() { }

        private IPAddress GetIp()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIPs)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    return address;
            }
            return null;
        }

        public ShipsClient(IPAddress server_ip, int port)
        {
            PORT = port;
            SERVER_IP = server_ip;
        }

        /// <summary>
        /// Establish connection if Client is not connected.
        /// </summary>
        public void EstablishConnection()
        {
            if(!ClientSocket.Connected)
                ClientSocket.Connect(new IPEndPoint(SERVER_IP, PORT));
        }

        private void SendString(string msg)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(msg);
            ClientSocket.Send(bytes);
        }

        public void SendRequest(GameRequest request)
        {
            string json = JsonConvert.SerializeObject(request);
            SendString(json);
        }

        public GameResponse ReciveResponse()
        {
            byte[] buffer = new byte[1024];
            ClientSocket.Receive(buffer);
            return DecodeResponse(buffer);
        }

        private GameResponse DecodeResponse(byte[] recivedBytes)
        {
            string json = Encoding.ASCII.GetString(recivedBytes).TrimEnd('\0');
            return JsonConvert.DeserializeObject<GameResponse>(json);
        }

        public GameResponse ListenForNotifyResponse()
        {
            return ReciveResponse();
        }

        public GameResponse SendRequestAndWaitForResponse(GameRequest request)
        {
            SendRequest(request);
            return ReciveResponse();
        }
    }
}
