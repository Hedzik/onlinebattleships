﻿
using OnlineBattleShipsClient.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Controls;

namespace OnlineBattleShipsClient.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public static MainWindowViewModel MAIN_WINDOW { get; set; }

        public Page CurrentPage { get; set; } = new GameLobbyPage();

        public MainWindowViewModel()
        {
            MAIN_WINDOW = this;
        }
    }


}