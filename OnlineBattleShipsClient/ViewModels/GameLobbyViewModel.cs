﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using OnlineBattleShipsClient.Pages;
using Server;

namespace OnlineBattleShipsClient.ViewModels
{
    public class GameLobbyViewModel : BaseViewModel
    {
        public string IpAddress { get; set; }
        public string Port { get; set; } = "13500";
        public string Nickname { get; set; } = "Hedziks";
        public string ErrorMessage { get; set; }
        public string Message { get; set; }
        public ICommand SubmitFormCommand { get; set; }

        private ShipsClient Client { get; set; }


        private bool IsFormValid
        {
            get
            {
                if (string.IsNullOrWhiteSpace(IpAddress) ||
                    string.IsNullOrWhiteSpace(Port))
                {
                    ErrorMessage = "The form cannot contain empty entries";
                    return false;
                }
                if (!IPAddress.TryParse(IpAddress, out IPAddress ip) ||
                    !int.TryParse(Port, out int port))
                {
                    ErrorMessage = "Please enter real IP and Port";
                    return false;
                }

                return true;
            }
        }

        public GameLobbyViewModel()
        {
            SubmitFormCommand = new RelayCommand(ProceedRegistartion);
        }

        private void LogError(string msg)
        {
            ClearMessages();
            ErrorMessage = msg;
        }

        private void LogMessage(string msg)
        {
            ClearMessages();
            Message = msg;
        }

        private void ClearMessages()
        {
            ErrorMessage = string.Empty;
            Message = string.Empty;
        }

        private async void ProceedRegistartion()
        {
            if (IsFormValid)
            {
                if(Client != null)
                    Client.ClientSocket.Dispose();
                Client = new ShipsClient(IPAddress.Parse(IpAddress), int.Parse(Port));


                LogMessage("Sending request...");
                GameResponse response = null;
                try
                {
                   response = await RegisterPlayer();
                }
                catch (SocketException ex)
                {
                    LogError($"Error while sending the request. ({ex.Message})");
                    return;
                }

                if (response != null && response.Success.HasValue && !response.Success.Value)
                {
                    LogError("Server rejected registration attempt, for detailed info see Server's log");
                    return;
                }

                LogMessage("Successed!");

                RegisterPlayerResponseArgs args = (response.Arguments as JObject).ToObject<RegisterPlayerResponseArgs>();
                MainWindowViewModel.MAIN_WINDOW.CurrentPage = new SetupBoardPage(new PlayerIdentity
                {
                    Id = args.AssociatedPlayerId,
                    Nickname = Nickname
                },Client);
            }
        }

        private async Task<GameResponse> RegisterPlayer()
        {
            GameResponse response = null;

            await Task.Run(() =>
            {
                Client.EstablishConnection();

                response = Client.SendRequestAndWaitForResponse(new GameRequest
                {
                    RequestType = GameRequestOrResponseType.RegisterPlayer,
                    Arguments = new RegisterPlayerRequestArgs
                    {
                        Nickname = Nickname
                    }
                });
            });

            return response;

        }
    }
}
