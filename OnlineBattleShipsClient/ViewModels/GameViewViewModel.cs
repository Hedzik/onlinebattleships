﻿using Newtonsoft.Json.Linq;
using Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OnlineBattleShipsClient
{
    public class GameViewViewModel : BaseViewModel
    {
        private ShipsClient Client { get; set; }

        public PlayerIdentity LocalPlayerIdentity { get; private set; }
        public string OponentNickname { get; private set; }

        private int BoardWidth { get; } = 10;
        private int BoardHeight { get; } = 10;

        private List<Cell> LocalPlayerShipCells { get; set; } = new List<Cell>();
        private List<Cell> OponentShipCells { get; set; } = new List<Cell>();

        private List<ShipTracker> ShipTrackers { get; set; } = new List<ShipTracker>();

        //Helper properties
        private bool IsItOurTurn { get; set; }
        private bool KeepReciving { get; set; } = true;
        private bool GameStarted { get; set; } = false;

        //Events
        public event Action<Cell> UpdateCell = null;
        public event Action<Cell> UpdateOponentCell = null;

        public event Action<string> LogMessage = null;

        public ICommand SurrenderCommand { get; set; }

        public GameViewViewModel(ShipsClient client, int boardWidth, int boardHeight, PlayerIdentity identity)
        {
            Client = client;
            BoardWidth = boardWidth;
            BoardHeight = boardHeight;
            LocalPlayerIdentity = identity;
            SurrenderCommand = new RelayCommand(Surrender);

            StartReciving();
        }

        public void SetupBoard(List<Cell> ShipCells)
        {
            LocalPlayerShipCells = ShipCells;
            Client.SendRequest(new GameRequest
            {
                RequestType = GameRequestOrResponseType.SetupBoard,
                Arguments = new SetupPlayerBoardRequestArgs
                {
                    SenderId = LocalPlayerIdentity.Id,
                    ShipCells = LocalPlayerShipCells.ToArray()
                }
            });
        }

        public void Surrender()
        {
            if (!GameStarted)
            {
                LogMessage("The game hasn't started yet. You can't surrender now!");
                return;
            }

            Client.SendRequest(new GameRequest
            {
                RequestType = GameRequestOrResponseType.Surrender,
                Arguments = new SurrenderRequestArgs
                {
                    SenderId = LocalPlayerIdentity.Id
                }
            });
        }

        public void CellClicked(Cell cell)
        {
            LogMessage("You can't shoot at your own board");
        }

        public void OponentCellClicked(Cell cell)
        {
            if (!GameStarted)
            {
                LogMessage("Game hasn't started yet");
                return;
            }
            else if (!IsItOurTurn)
            {
                LogMessage("That's not your turn!");
                return;
            }
            else if (cell.Type == CellType.AlreadyShotDownShipPart || cell.Type == CellType.EmptyAndAlreadyShotDown)
            {
                LogMessage("You have already shot this cell!");
                return;
            }

            Client.SendRequest(new GameRequest
            {
                RequestType = GameRequestOrResponseType.MakeAMove,
                Arguments = new MakeAMoveRequestArgs
                {
                    SenderId = LocalPlayerIdentity.Id,
                    X = cell.X,
                    Y = cell.Y
                }
            });
        }

        private void ResponseRecived(GameResponse response)
        {
            switch (response.ResponseType)
            {
                case GameRequestOrResponseType.SetupBoard:
                    LogMessage("Everything ready, waiting for oponent...");
                    break;
                case GameRequestOrResponseType.MakeAMove:
                    if (response.Success.HasValue && !response.Success.Value)
                    {
                        LogMessage("It's not your turn!");
                        return;
                    }
                    MakeAMoveResponseArgs makeAMoveResponseArgs = (response.Arguments as JObject).ToObject<MakeAMoveResponseArgs>();
                    UpdateOponentCell(new Cell
                    {
                        Type = makeAMoveResponseArgs.ShotCell,
                        X = makeAMoveResponseArgs.X,
                        Y = makeAMoveResponseArgs.Y
                    });
                    LogMessage("You have successfully made your move!");
                    break;
                case GameRequestOrResponseType.NotifyTurnChanged:
                    //In case it's first move of the game we don't have information about past move, that's destination of this "if"
                    if (response.Arguments != null)
                    {
                        NotifyTurnChangedResponseArgs notifyTurnChangedResponseArgs = (response.Arguments as JObject).ToObject<NotifyTurnChangedResponseArgs>();
                        UpdateCell(new Cell
                        {
                            Type = notifyTurnChangedResponseArgs.CellType,
                            X = notifyTurnChangedResponseArgs.X,
                            Y = notifyTurnChangedResponseArgs.Y
                        });
                    }
                    IsItOurTurn = true;
                    LogMessage("Your turn!");
                    break;
                case GameRequestOrResponseType.NotifyGameStarted:
                    OponentNickname = (response.Arguments as JObject).ToObject<NotifyGameStartedResponseArgs>().OponentsNickname;
                    GameStarted = true;
                    LogMessage("Oponent ready, starting the game");
                    break;
                case GameRequestOrResponseType.Surrender:
                    SurrenderResponseArgs surrenderResponseArgs = (response.Arguments as JObject).ToObject<SurrenderResponseArgs>();
                    GameStarted = false;
                    KeepReciving = false;
                    if (surrenderResponseArgs.Winner == LocalPlayerIdentity.Nickname)
                        LogMessage($"{surrenderResponseArgs.Loser} decided to surrender the game instantly. You won!");
                    else
                        LogMessage($"You've lost!");

                    break;
                case GameRequestOrResponseType.Error:
                    LogMessage("An server-side error occured while proceeding the request.");
                    break;
                case GameRequestOrResponseType.NotifyGameOver:
                    NotifyGameOverResponseArgs notifyGameOverArgs = (response.Arguments as JObject).ToObject<NotifyGameOverResponseArgs>();
                    //Avoid making more turns
                    GameStarted = false;
                    KeepReciving = false;

                    if (notifyGameOverArgs.Winner == LocalPlayerIdentity.Nickname)
                        LogMessage($"The game is over! You won!");
                    else
                        LogMessage($"{notifyGameOverArgs.Winner} won the game!");
                    break;
                default:
                    LogMessage($"Unexpected response type({response.ResponseType.ToString()})");
                    break;
            }
        }

        private async void StartReciving()
        {
            await Task.Run(() =>
            {
                GameResponse response;
                while (KeepReciving)
                {
                    response = Client.ReciveResponse();
                    ResponseRecived(response);
                }
            });
        }
    }
}
